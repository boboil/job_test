<?php

class User
{
    public static function checkUserData($facebook_id)
    {
        $db = DB::getConnection();
        $result = $db->query("SELECT * FROM `users` WHERE `facebook_id` = $facebook_id");
        $user = $result->fetch();
        if ($user) {
            return $user;
        }
        return false;
    }
    public static function getUserFacebookId($id)
    {
        $db = DB::getConnection();
        $result = $db->query("SELECT * FROM `users` WHERE `id` = $id");
        $user = $result->fetch();
        if ($user) {
            return $user['facebook_id'];
        }
        return false;
    }
    public static function getUserFacebookAccesToken($id)
    {
        $db = DB::getConnection();
        $result = $db->query("SELECT * FROM `users` WHERE `id` = $id");
        $user = $result->fetch();
        if ($user) {
            return $user['accessToken'];
        }
        return false;
    }
    public static function auth($facebook_id)
    {
        session_start();
        $_SESSION['user'] = $facebook_id;
    }

    public static function register($facebook_id, $name, $accessToken)
    {
        $db = DB::getConnection();
        $result = $db->query( "INSERT INTO users (facebook_id, name, accessToken) VALUES ('$facebook_id', '$name', '$accessToken');");
        return true;
    }
    public static function getUsers()
    {
        $db = DB::getConnection();

        $users = [];
        $result = $db->query("SELECT * FROM `users`");
        $i = 0;
        while ($row = $result->fetch()){
            $users[$i]['id'] = $row['id'];
            $users[$i]['name'] = $row['name'];
            $users[$i]['facebook_id'] = $row['facebook_id'];
            $users[$i]['accessToken'] = $row['accessToken'];
            $i++;
        }
        return $users;
    }
    public static function getUsersWithTelegram()
    {
        $db = DB::getConnection();

        $users = [];
        $result = $db->query("SELECT * FROM `users` WHERE `telegram_id` IS NOT NULL");
        $i = 0;
        while ($row = $result->fetch()){
            $users[$i]['id'] = $row['id'];
            $users[$i]['name'] = $row['name'];
            $users[$i]['facebook_id'] = $row['facebook_id'];
            $users[$i]['accessToken'] = $row['accessToken'];
            $users[$i]['telegram_id'] = $row['telegram_id'];
            $i++;
        }
        return $users;
    }
    public static function updateTelegramId($t_id, $f_id)
    {
        $db = DB::getConnection();
        $sql = "UPDATE users
            SET 
                telegram_id = :telegram_id
            WHERE users . facebook_id = :facebook_id";
        $result = $db->prepare($sql);
        $result->bindParam(':telegram_id', $t_id, PDO::PARAM_INT);
        $result->bindParam(':facebook_id', $f_id, PDO::PARAM_INT);
        return $result->execute();
    }
}

?>