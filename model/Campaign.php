<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17.07.2020
 * Time: 9:31
 */

require_once (__DIR__  .'/../components/DB.php');

class Campaign
{
    public static function add($name, $campaign_id, $status)
    {
        $db = DB::getConnection();
        $result = $db->query( "INSERT INTO campaigns (name, campaign_id, status) VALUES ('$name', '$campaign_id', '$status');");
        return true;
    }
    public static function updateStatus($campaign_id, $status)
    {
        $db = DB::getConnection();
        $sql = "UPDATE campaigns
            SET 
                status = :status
            WHERE campaigns . campaign_id = :campaign_id";
        $result = $db->prepare($sql);
        $result->bindParam(':campaign_id', $campaign_id, PDO::PARAM_INT);
        $result->bindParam(':status', $status, PDO::PARAM_STR);
        return $result->execute();
    }
    public static function checkCampaignData($campaign_id)
    {
        $db = DB::getConnection();
        $result = $db->query("SELECT * FROM `campaigns` WHERE `campaign_id` = $campaign_id");
        $campaign = $result->fetch();
        if ($campaign) {
            return $campaign;
        }
        return false;
    }



    public static function send_request_get($url, $params) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
    }
}