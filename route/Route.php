<?php

class Route
{
    private $routes;

    /**
     * @return mixed
     */
    public function __construct()
    {
        $routePath = ROOT.'/route/routes.php';
        $this->routes = include($routePath);
    }

    private function getUri()
    {
        if (!empty($_SERVER['REQUEST_URI'])){
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    public function run()
    {
        $uri = $this->getUri();
        foreach ($this->routes as $uriPattern => $path)
        {

            if (preg_match("~$uriPattern~", $uri))
            {

                $internalRoute = preg_replace("~$uriPattern~", $path, $uri);
                $segments = explode('/', $internalRoute);

                $controllerName = ucfirst(array_shift($segments).'Controller');


                $action = array_shift($segments);

                $paramets = $segments;

                $controllerFile = ROOT . '/controller/' . $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once ($controllerFile);
                }

                $controllerObject = new $controllerName;

                $result = call_user_func(array($controllerObject, $action), $paramets);

                if ($result != null){
                    break;
                }
            }
        }
    }
}