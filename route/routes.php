<?php
return array(
    'index/code=([a-zA-Z0-9 &_.,!()+=`,"@$#%*-]+)' =>  'task/index_2/$1',
    '' =>  'task/index',
    'register' =>  'task/register',
    'business/([0-9]+)' =>  'parser/business/$1',
    'user-list'=>'parser/userList',
    'new-user'=>'parser/addNewUser',
    'add'=>'task/add',
    'politics'=>'task/politics',
    'authorize'=>'task/authorize',
    'addTelegraMessage'=>'task/addTelegraMessage',

);