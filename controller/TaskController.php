<?php

include_once __DIR__. '/../model/User.php';
include_once __DIR__. '/../model/Campaign.php';
require_once __DIR__ . '/../vendor/autoload.php';

use FacebookAds\Api;
use FacebookAds\Logger\CurlLogger;
use FacebookAds\Object\AdAccount;
//use FacebookAds\Object\Campaign;
use FacebookAds\Object\Fields\CampaignFields;
use FacebookAds\Object\Fields\AdAccountFields;
use Facebook\Facebook;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;




class TaskController
{

    public function index()
    {
        require_once (ROOT .'/views/index.php');

    }

    public function register()
    {
        $facebook_id = $_POST['facebook_id'];
        $name =  $_POST['name'];
        $accessToken = $_POST['accessToken'];
        $user =  User::checkUserData($facebook_id);

        if (!$user){
            $user = new User();
            $user->register($facebook_id,$name,$accessToken);
        }


    }
    public static function send_request_get($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;

    }
    public function add()
    {
        $accessToken = $_POST['accessToken'];
        $url = 'https://graph.facebook.com/v5.0/me?fields=id,first_name,last_name&access_token='.$accessToken;

        $user = json_decode($this->send_request_get($url));
        $id = $user->id;
        $name = $user->first_name .' '.$user->last_name;
        $user =  User::checkUserData($id);
        $message = 'Такой пользователь уже существует, попробуйте с другим токеном спасибо!';
        if (!$user){
            User::register($id, $name, $accessToken);
            $message = 'Успешно добавили пользователя, спасибо!';
        }
        $data = [
            'next' => '/user-list',
            'message' => $message
        ];
        $output = json_encode($data);
        echo $output;
    }

    public function userList()
    {
        $users = User::getUsers();
        require_once (ROOT .'/views/user_list.php');
        return true;
    }

    public function authorize()
    {
        $facebook_id = $_POST['facebook_id'];
        $name =  $_POST['name'];
        $accessToken = $_POST['accessToken'];
        $user =  User::checkUserData($facebook_id);
        if (!$user){
            $user = new User();
            $user->register($facebook_id,$name,$accessToken);
        }
        User::auth($facebook_id);
        $message = 'Спасибо за авторизацию';
        $data = [
            'next' => '/user-list',
            'message' => $message
        ];
        $output = json_encode($data);
        echo $output;
    }

    public function politics()
    {
        require_once (ROOT .'/views/politics.php');
    }
    public function addTelegraMessage()
    {
        print_r($_POST);
        die();
    }
    
}