<?php
include_once ROOT. '/model/User.php';
include_once ROOT. '/model/Campaign.php';
require_once __DIR__ . '/../vendor/autoload.php';


class ParserController
{
    public function __construct()
    {
        session_start();
        $facebook_id = $_SESSION['user'];
        $user = User::checkUserData($facebook_id);
        if ($user){
            return true;
        }else{
            print_r('Авторизуйтесь пожайлуста');
            die();
        }
    }
    public function userList()
    {
        $users = User::getUsers();
        require_once (ROOT .'/views/user_list.php');
        return true;
    }

    public function addNewUser()
    {
        require_once (ROOT .'/views/add_new_user.php');
        return true;
    }

    public function business($id)
    {
        $t_url = 'https://api.telegram.org/bot1134111813:AAFBtQ3S4KU3gibeGvNT4Nit_KvZEVx--oo/sendMessage';
        $ad_account_id = User::getUserFacebookId($id[0]);
        $access_token = User::getUserFacebookAccesToken($id[0]);
        $url = 'https://graph.facebook.com/v7.0/'.$ad_account_id.'/adaccounts?fields=account_id&access_token='.$access_token;
        $data = json_decode($this->send_request_get($url));
        $fbId = $data->data[0]->id;
        if (!$fbId){
            print_r('Нет рекламного акаунта');
            die();
        }else{
            $url = 'https://graph.facebook.com/v7.0/'.$fbId.'/ads?fields=name,status&access_token='.$access_token;
            $ads = json_decode($this->send_request_get($url));
            if (empty($ads->data)){
                print_r('Нет рекламных объявлений');
                die();
            }
            foreach ($ads->data as $ad){
                $status = $ad->status;
                $name = $ad->name;
                $id = $ad->id;
                $params = [
                    'chat_id' => '61995503',
                    'text' => 'Объявление'.$name. ' - '.$status
                ];
                $cap = Campaign::checkCampaignData($id);
                if (!$cap){
                    Campaign::add($name, $id, $status);
                    Campaign::send_request_get($t_url, $params);
                }elseif ($cap['status'] != $status){
                    Campaign::send_request_get($t_url, $params);
                    Campaign::updateStatus($id,$status);
                }
                echo $name .' статус -'. $status .PHP_EOL;
            }

        }

        return true;
    }

    public function send_request_get($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_HEADER, false);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;

    }
}