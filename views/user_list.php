<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>User list</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<div class="container">
    <div class="col-lg-12">
        <a href="/new-user">Добавить нового пользователяу</a><br/>
        <a href="https://t.me/Test1JobBot?start=<?php echo $_SESSION['user'] ?>">Получать уведомления</a>
        <table id="table_id" class="display" data-page-length='3'>
            <thead>
            <tr>
                <th>Имя пользователя</th>
                <th>facebook_id</th>
                <th>Токен</th>
                <th>Проверить статус объявлений</th>
            </tr>
            </thead>
            <tbody>

            <?php foreach ($users as $user) { ?>
                <tr>
                    <td><?php echo $user['name'] ?></td>
                    <td><?php echo $user['facebook_id'] ?></td>
                    <td><?php echo substr($user['accessToken'], 0, 50) ?></td>
                    <td><a href="/business/<?php echo $user['id'] ?>">Проверить статус объявлений</a></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
<footer>
    <script
        src="https://code.jquery.com/jquery-2.2.4.min.js"
        integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44="
        crossorigin="anonymous">
    </script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.20/datatables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="/views/scripts/main.js"></script>
    <script src="/views/scripts/script.js"></script>
    <script>
        $(document).ready( function () {
            $('#table_id').DataTable();
        } );
    </script>
</footer>
</body>