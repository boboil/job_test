var addNewUser = document.getElementById("addNewUser"),
    authorizationForm = document.getElementById("authorizationForm");
if (typeof(addNewUser) != "undefined" && addNewUser !== null){
    addNewUser.addEventListener('submit', function(e) {
        e.preventDefault();
        var accessToken = document.getElementById('accessToken').value;
        const request = new XMLHttpRequest();
        const url = "/add";
        const params = "accessToken=" + accessToken;

        request.open("POST", url, true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.addEventListener("readystatechange", () => {

            if(request.readyState === 4 && request.status === 200) {
                var response = JSON.parse(request.responseText);
                alert(response.message);
                window.location.href = response.next;
            }
        });
        request.send(params);
    });
}

if (typeof(authorizationForm) != "undefined" && authorizationForm !== null){
    authorizationForm.addEventListener('submit', function (e) {
        e.preventDefault();
        var accessToken = document.getElementById('inputAcessToken').value,
            facebookId = document.getElementById('inputFacebookId').value,
            inputName = document.getElementById('inputName').value;
        const request = new XMLHttpRequest();
        const url = "/authorize";
        const params = "facebook_id=" + facebookId+ "&name=" + inputName+ "&accessToken=" + accessToken;

        request.open("POST", url, true);
        request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        request.addEventListener("readystatechange", () => {

            if(request.readyState === 4 && request.status === 200) {
                var response = JSON.parse(request.responseText);
                alert(response.message);
                window.location.href = response.next;
            }
        });
        request.send(params);
    });
}
