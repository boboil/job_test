function statusChangeCallback(response) {  // Called with the results from FB.getLoginStatus().
    console.log('statusChangeCallback');
    console.log(response);                   // The current login status of the person.
    if (response.status === 'connected') {
        var accessToken = response.authResponse.accessToken;
        testAPI(accessToken);
    } else {                                 // Not logged into your webpage or we are unable to tell.
        document.getElementById('status').innerHTML = 'Please log ' +
            'into this webpage.';
    }
}


function checkLoginState() {
    FB.getLoginStatus(function(response) {
        statusChangeCallback(response);
    });
}


window.fbAsyncInit = function() {
    FB.init({
        appId      : '1319553704781941',
        cookie     : true,                     // Enable cookies to allow the server to access the session.
        xfbml      : true,                     // Parse social plugins on this webpage.
        version    : 'v7.0'           // Use this Graph API version for this call.
    });


    FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
        statusChangeCallback(response);        // Returns the login status.
    });
};

function testAPI(accessToken) {                      // Testing Graph API after login.  See statusChangeCallback() for when this call is made.
    FB.api('/me', function(response) {
        console.log(accessToken);
        registerUser(response.id, response.name, accessToken);
        document.getElementById('status').innerHTML =
            'Thanks for logging in, ' + response.name + '!'+
            '<a href="/user-list">Список пользователей</a>';

    });
}
function registerUser(facebook_id, name, accessToken) {
    const request = new XMLHttpRequest();
    const url = "/register";
    const params = "facebook_id=" + facebook_id+ "&name=" + name+ "&accessToken=" + accessToken;

    request.open("POST", url, true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

    request.send(params);


}
function addTelegraMessage(facebook_id) {
    const request = new XMLHttpRequest();
    const url = "/addTelegraMessage";
    const params = "facebook_id=" + facebook_id;
    request.open("POST", url, true);
    request.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    request.addEventListener("readystatechange", () => {

        if(request.readyState === 4 && request.status === 200) {
            // var response = JSON.parse(request.responseText);
            // alert(response.message);
        }
    });
    request.send(params);
}
