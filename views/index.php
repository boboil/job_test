<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Test Facebook</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="/views/css/style.css">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <form class="form-horizontal" id="authorizationForm">
                <span class="heading">АВТОРИЗАЦИЯ</span>
                <div class="form-group">
                    <input type="number" class="form-control" id="inputFacebookId" placeholder="Facebook id" required>
                    <i class="fa fa-facebook"></i>
                </div>
                <div class="form-group">
                    <input type="text" class="form-control" id="inputName" placeholder="Name" required>
                    <i class="fa fa-user"></i>
                </div>
                <div class="form-group">
                    <textarea class="form-control" name="inputAcessToken" id="inputAcessToken" cols="20" placeholder="AcessToken"></textarea>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-default">ВХОД</button>
                </div>
            </form>
        </div>
    </div>
</div>
<footer>
    <script src="/views/scripts/script.js"></script>
</footer>
</body>