<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17.07.2020
 * Time: 17:46
 */

include_once __DIR__. '/../model/Campaign.php';
include_once __DIR__. '/../model/User.php';
require_once __DIR__ . '/../vendor/autoload.php';
include_once __DIR__. '/../controller/TaskController.php';


$url = 'https://api.telegram.org/bot1134111813:AAFBtQ3S4KU3gibeGvNT4Nit_KvZEVx--oo/getUpdates';
$result = file_get_contents($url);
$result = json_decode($result, true);

foreach ($result['result'] as $value) {
    if (isset($value['message']['entities'])) {
        $f_id = (int)str_replace('/start ', '', $value['message']['text']);
        $t_id = $value['message']['from']['id'];
        $user = User::checkUserData($f_id);
        if ($user) {
            $a = User::updateTelegramId($t_id, $f_id);
        }

    }
}
$t_url = 'https://api.telegram.org/bot1134111813:AAFBtQ3S4KU3gibeGvNT4Nit_KvZEVx--oo/sendMessage';
$users = User::getUsersWithTelegram();
    foreach ($users as $user){
        $ad_account_id = $user['facebook_id'];
        $access_token = $user['accessToken'];
        $url = 'https://graph.facebook.com/v7.0/' . $ad_account_id . '/adaccounts?fields=account_id&access_token=' . $access_token;
        $data = json_decode(TaskController::send_request_get($url));
        $fbId = $data->data[0]->id;
        if (!$fbId) {
            print_r('Нет рекламного акаунта');
            die();
        } else {
            $url = 'https://graph.facebook.com/v7.0/' . $fbId . '/ads?fields=name,status&access_token=' . $access_token;
            $ads = json_decode(TaskController::send_request_get($url));
            if (empty($ads->data)) {
                print_r('Нет рекламных объявлений');
                die();
            }
            foreach ($ads->data as $ad) {
                $status = $ad->status;
                $name = $ad->name;
                $id = $ad->id;
                $params = [
                    'chat_id' => $user['telegram_id'],
                    'text' => 'Объявление' . $name . ' - ' . $status
                ];
                $cap = Campaign::checkCampaignData($id);
                if (!$cap) {
                    Campaign::add($name, $id, $status);
                    Campaign::send_request_get($t_url, $params);
                } elseif ($cap['status'] != $status) {
                    Campaign::send_request_get($t_url, $params);
                    Campaign::updateStatus($id, $status);
                }
                echo $name . ' статус -' . $status . PHP_EOL;
            }
    }
}