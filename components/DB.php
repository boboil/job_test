<?php


class DB
{
    public static function getConnection()
    {
        $paransPath = __DIR__  .'/../config.php';
        $params = include($paransPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['database']}";
        $db = new PDO($dsn, $params['user'], $params['password']);
        return $db;
    }
}